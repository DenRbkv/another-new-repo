import 'package:flutter/material.dart';

final TextStyle dateStyle = TextStyle(color: Colors.grey, fontSize: 13);
final TextStyle titleStyle = TextStyle(fontSize: 16, fontWeight: FontWeight.bold);